<?php

/**
 * @file SanitizeEditorSelectableReviewAttachmentsGridHandler.inc.php
 *
 * @class SanitizeEditorSelectableReviewAttachmentsGridHandler
 * @ingroup plugins_generic_sanitize
 *
 * @brief Custom selectable review attachment grid (editor's perspective) with sanitize action.
 */

import('lib.pkp.controllers.grid.files.attachment.EditorSelectableReviewAttachmentsGridHandler');
import('plugins.generic.sanitize.SanitizeGridHandlerTrait');

class SanitizeEditorSelectableReviewAttachmentsGridHandler extends EditorSelectableReviewAttachmentsGridHandler {
	use SanitizeGridHandlerTrait;
}
