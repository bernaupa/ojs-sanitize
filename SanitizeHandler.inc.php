<?php

/**
 * @file SanitizeHandler.inc.php
 *
 * @class SanitizeHandler
 * @ingroup plugins_generic_sanitize
 *
 * @brief Handler for sanitizing a submission file.
 */

import('classes.handler.Handler');

class SanitizeHandler extends Handler {

	/** @var SubmissionFile the file to sanitize */
	private $_submissionFile;

	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();
		$this->addRoleAssignment(
			array(ROLE_ID_MANAGER),
			array('sanitizeFile')
		);
	}

	//
	// Implement methods from PKPHandler
	//
	/**
	 * @copydoc PKPHandler::authorize()
	 */
	function authorize($request, &$args, $roleAssignments) {
		import('lib.pkp.classes.security.authorization.SubmissionFileAccessPolicy');
		$this->addPolicy(new SubmissionFileAccessPolicy($request, $args, $roleAssignments, SUBMISSION_FILE_ACCESS_MODIFY));

		return parent::authorize($request, $args, $roleAssignments);
	}

	/**
	 * Configure handler.
	 * @param $request PKPRequest
	 */
	function initialize($request) {
		parent::initialize($request);

		// retrieve the authorized submission file
		$this->_submissionFile = $this->getAuthorizedContextObject(ASSOC_TYPE_SUBMISSION_FILE);
	}

	//
	// Public handler methods
	//
	/**
	 * Sanitize a file.
	 * @param $args array
	 * @param $request Request
	 */
	function sanitizeFile($args, $request) {
		assert(is_a($this->_submissionFile, 'SubmissionFile'));

		if ($this->_submissionFile->getDocumentType() == DOCUMENT_TYPE_PDF) {
			// can only sanitize PDFs
			SanitizePlugin::sanitizePDF($this->_submissionFile);
		}

		return DAO::getDataChangedEvent();
	}
}