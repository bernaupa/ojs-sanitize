<?php

/**
 * @defgroup plugins_generic_sanitize Sanitize plugin
 */

/**
 * @file index.php
 *
 * @ingroup plugins_generic_sanitize
 * @brief Wrapper for Sanitize plugin.
 *
 */
require_once('SanitizePlugin.inc.php');

return new SanitizePlugin();
