<?php

/**
 * @file SanitizeSendReviewForm.inc.php
 *
 * @class SanitizeSendReviewsForm
 * @ingroup plugins_generic_sanitize
 *
 * @brief Form to request additional work from the author (Request revisions or
 *  resubmit for review), or to decline the submission.
 */

import('lib.pkp.controllers.modals.editorDecision.form.SendReviewsForm');
import('plugins.generic.sanitize.SanitizeTrait');

class SanitizeSendReviewsForm extends SendReviewsForm {
	// insert sanitization
	use SanitizeTrait;
}
