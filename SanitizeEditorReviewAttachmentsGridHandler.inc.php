<?php

/**
 * @file SanitizeEditorReviewAttachmentsGridHandler.inc.php
 *
 * @class SanitizeEditorReviewAttachmentsGridHandler
 * @ingroup plugins_generic_sanitize
 *
 * @brief Custom Editor's view of the Review Attachments with sanitize action.
 */

import('lib.pkp.controllers.grid.files.attachment.EditorReviewAttachmentsGridHandler');
import('plugins.generic.sanitize.SanitizeGridHandlerTrait');

class SanitizeEditorReviewAttachmentsGridHandler extends EditorReviewAttachmentsGridHandler {
	use SanitizeGridHandlerTrait;
}
