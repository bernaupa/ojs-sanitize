<?php

/**
 * @file SanitizeFileLinkAction.inc.php
 *
 * @class SanitizeFileLinkAction
 * @ingroup plugins_generic_sanitize
 *
 * @brief An action to sanitize a submission file.
 */

import('lib.pkp.controllers.api.file.linkAction.FileLinkAction');

class SanitizeFileLinkAction extends FileLinkAction {
	/**
	 * Constructor
	 * @param $request Request
	 * @param $submissionFile SubmissionFile the submission file to be sanitized
	 * @param $stageId int (optional)
	 */
	function __construct($request, $submissionFile, $stageId) {
		$router = $request->getRouter();

		parent::__construct(
			'sanitizeFile',
			new AjaxAction(
					$router->url($request, null, 'plugins.generic.sanitize.SanitizeHandler', 'sanitizeFile', null, $this->getActionArgs($submissionFile, $stageId))
				),
			__('plugins.generic.sanitize.grid.action.sanitize'),
			'sanitize',
			__('plugins.generic.sanitize.grid.action.sanitizeFile')
		);
	}
}