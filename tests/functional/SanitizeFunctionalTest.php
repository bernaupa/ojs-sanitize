<?php

/**
 * @file tests/functional/SanitizeFunctionalTest.php
 *
 *
 * @class SanitizeFunctionalTest
 * @ingroup plugins_generic_sanitize
 *
 * @brief Functional tests for the Sanitize plugin.
 */

import('tests.ContentBaseTestCase');

class SanitizeFunctionalTest extends ContentBaseTestCase {
	/** @var string Title of a hand-picked submission in review stage from test database */
	protected $title = 'Developing efficacy beliefs in the classroom';

	/** @var string The author username for submission */
	protected $author = 'dsokoloff';

	/** @var string The reviewer username for submission */
	protected $reviewer = 'agallego';
	/** @var string The reviewer full name for submission */
	protected $reviewerFullName = 'Adela Gallego';

	/** @var string The editor username in charge of submission */
	protected $editor = 'dbarnes';

	/** @var string The reviewer attachment, not anonymized */
	protected $reviewerFile = 'reviewerfile.pdf';

	/**
	 * @copydoc WebTestCase::getAffectedTables
	 */
	protected function getAffectedTables() {
		return PKP_TEST_ENTIRE_DB;
	}

	/**
	 * @see WebTestCase::setUp()
	 */
	protected function setUp() {
		parent::setUp();

		$this->start();

		$this->enablePlugin();

		// create a review with reviewer file
		$file = \dirname(__FILE__) . '/' . $this->reviewerFile;
		$this->performReview($this->reviewer, null, $this->title, 'Revisions Required', 'comments', $file);
	}

	/**
	 * Enable the plugin
	 */
	private function enablePlugin() {
		$this->logIn('admin', 'admin');

		$this->waitForElementPresent($selector = 'link=Website');
		$this->clickAndWait($selector);
		$this->click('link=Plugins');

		// find the plugin
		$this->waitForElementPresent($selector = '//input[starts-with(@id, \'select-cell-sanitizeplugin-enabled\')]');
		// enable if necessary
		if (!$this->isChecked($selector)) {
			// do enable the plugin
			$this->click($selector);
			$this->waitForElementPresent('//div[contains(.,\'The plugin "Sanitize" has been enabled.\')]');
		}

		// open settings dialog
		$this->waitForElementPresent($selector='css=a[id^=component-grid-settings-plugins-settingsplugingrid-category-generic-row-sanitizeplugin-settings-button]');
		$this->click($selector);

		// set command for sanitizing reviewer PDF file
		$this->waitForElementPresent($selector='css=[id^=commandPdf-]');
		$this->type($selector, 'qpdf --linearize --empty --pages {$in}  1-z -- {$out} 2>&1');

		// save and check notification
		$this->waitForElementPresent($selector='css=button:contains(\'Save\')');
		$this->click($selector);
		$this->waitForElementPresent('//div[contains(.,\'Sanitize plugin settings saved\')]');

		$this->logOut();
	}

	/**
	 * Perform a review with review file.
	 *
	 * @see PKPContentBaseTestCase::performReview()
	 */
	public function performReview($username, $password, $title, $recommendation = null, $comments = 'Here are my review comments.', $file = null) {
		if ($password===null) $password = $username . $username;
		$this->logIn($username, $password);

		$xpath = '//div[normalize-space(text())=\'' . $title . '\']';
		$this->waitForElementPresent($xpath);
		$this->click($xpath);

		$this->waitForElementPresent($selector='//button[text()=\'Accept Review, Continue to Step #2\']');
		$this->click('//input[@id=\'privacyConsent\']');
		$this->click($selector);

		$this->waitForElementPresent($selector='//button[text()=\'Continue to Step #3\']');
		$this->click($selector);
		$this->waitForElementPresent('css=[id^=comments-]');
		$this->typeTinyMCE('comments', $comments);

		if ($recommendation !== null) {
			$this->select('id=recommendation', 'label=' . $this->escapeJS($recommendation));
		}

		// upload reviewer file
		if ($file !== null) {
			$this->click('css=[id^=component-grid-files-attachment-reviewerreviewattachmentsgrid-addFile-button-]');

			$this->waitForElementPresent('css=div.pkp_modal');
			$this->uploadFile($file);
			$this->waitForElementPresent('css=button[id=continueButton]:enabled');
			$this->click('//button[text()=\'Continue\']');
			$this->waitForElementPresent('css=div.pkp_uploadedFile_summary');
			$this->click('//button[text()=\'Continue\']');
			$this->waitForElementPresent('//h2[text()=\'File Added\']');
			$this->click('//button[text()=\'Complete\']');
			$this->waitForElementNotPresent('css=div.pkp_modal');
		}

		$this->waitForElementPresent($selector='//button[text()=\'Submit Review\']');
		$this->click($selector);
		$this->waitForElementPresent($selector='link=OK');
		$this->click($selector);
		$this->waitForElementPresent('//h2[contains(text(), \'Review Submitted\')]');
		$this->waitJQuery();
		$this->logOut();
	}

	/**
	 * Record an editorial decision, sharing review files with authors.
	 *
	 * @see PKPContentBaseTestCase::recordEditorialDecision()
	 */
	protected function recordEditorialDecision($decision) {
		$this->waitForElementPresent($selector='//a[contains(.,\'' . $this->escapeJS($decision) . '\')]');
		$this->click($selector);

		$this->waitForElementPresent($selector='//button[contains(.,\'Record Editorial Decision\')]');

		// share all files with author
		$checkboxes = '//input[@name=\'selectedAttachments[]\']';
		for ($i = $this->getXpathCount($checkboxes); $i > 0 ; $i--) {
			$this->click('xpath=(' . $checkboxes . ')[' . $i . ']');
		}

		$this->click($selector);
		$this->waitForElementNotPresent('css=div.pkp_modal_panel'); // pkp/pkp-lib#655
	}

	/**
	 * Test that PDFs as reviewer files are actually anonymized.
	 */
	function testSanitize() {
		$this->assertFalse($this->sanitized(\dirname(__FILE__) . '/reviewerfile.pdf'));

		// send reviews to the author
		$this->findSubmissionAsEditor($this->editor, null, $this->title);
		$this->recordEditorialDecision('Request Revisions');
		$this->logOut();

		// select the submission as author
		// see PKPContentBaseTestCase::findSubmissionAsEditor()
		$this->logIn($this->author);
		$this->waitForElementPresent('css=#dashboardTabs');
		$this->click('css=[name=myQueue]');
		$xpath = '//div[contains(text(),\'' . $this->title . '\')]';
		$this->waitForElementPresent($xpath);
		$this->click($xpath);

		$tempfile = tempnam(sys_get_temp_dir(), 'tst');
		$this->downloadReviewerAttachment($this->reviewerFile, $tempfile);
		$this->assertTrue($this->sanitized($tempfile));
		unlink($tempfile);

		// manipulate the file url to try accessing the original reviewer file (unsanitized)
		$downloadLinkHref = $this->getAttribute('//a[contains(text(),\'' . $this->reviewerFile . '\')]/@href');
		$this->assertContains('revision=2', $downloadLinkHref);
		$downloadLinkHref = str_replace('revision=2', 'revision=1', $downloadLinkHref);
		$this->open($downloadLinkHref);
		$response = $this->getBodyText();
		$this->assertJson($response);
		$responseJson = json_decode($response);
		$this->assertFalse($responseJson->{'status'});
		$this->assertContains('The current role does not have access to this operation.', $responseJson->{'content'});
	}

	/**
	 * Use the grid row action to sanitize the reviewer file.
	 */
	protected function sanitizeAction() {
		$rowSelector = 'css=tr:contains(\''. $this->reviewerFile .'\')';

		// check the file is not sanitized
		$this->waitForElementPresent($rowSelector);
		$tempfile = tempnam(sys_get_temp_dir(), 'tst');
		$this->downloadSubmissionFile($this->reviewerFile, $tempfile, $this->editor);
		$this->assertFalse($this->sanitized($tempfile));
		unlink($tempfile);

		// apply sanitization
		$this->waitForElementPresent($selector = $rowSelector . ' + tr a:contains(\'Sanitize\')');
		$this->click($selector);
		$this->waitJQuery();

		$this->waitForElementPresent($rowSelector);

		// check the file has been sanitized
		$tempfile = tempnam(sys_get_temp_dir(), 'tst');
		$this->downloadSubmissionFile($this->reviewerFile, $tempfile, $this->editor);
		$this->assertTrue($this->sanitized($tempfile));
		unlink($tempfile);

		// look up sanitization log entry in file history ('Information Center' popup)
		$this->waitForElementPresent($selector = $rowSelector . ' + tr a:contains(\'More Information\')');
		$this->click($selector);
		$this->waitForElementPresent('css=[id^=component-grid-eventlog-submissionfileeventloggrid-row]:contains(\'An anonymized revision\')');
		$this->click('css=div.pkp_modal:contains(\'Information Center\') a.pkpModalCloseButton');
		$this->waitJQuery();
	}

	/**
	 * Test an editor can sanitize reviewer files (Review Attachments Grid).
	 */
	function testSanitizeActionReviewAttachmentsGrid() {
		$this->findSubmissionAsEditor($this->editor, null, $this->title);

		// 'Review Details' popup
		$this->waitForElementPresent($selector = 'css=tr:contains(\''. $this->reviewerFullName .'\') + tr a:contains(\'Review Details\')');
		$this->click($selector);
		$this->waitForElementPresent('css=div.pkp_modal');

		$this->sanitizeAction();
	}

	/**
	 * Test an editor can sanitize reviewer files (Selectable Review Attachments Grid).
	 */
	function testSanitizeActionSelectableReviewAttachmentsGrid() {
		$this->findSubmissionAsEditor($this->editor, null, $this->title);

		// editor decision modals ('Request Revisions' popup)
		$this->waitForElementPresent($selector='//a[contains(.,\'Request Revisions\')]');
		$this->click($selector);
		$this->waitForElementPresent('css=div.pkp_modal');

		$this->sanitizeAction();
	}


	/**
	 * Check whether the author name is leaked in PDF metadata.
	 *
	 * @param type $filename the path to the file to test
	 * @return boolean FALSE if not sanitized or error, TRUE otherwise
	 */
	private function sanitized($filename) {
		$out = shell_exec('pdfinfo "' . $filename . '"');
		return isset($out) && preg_match('/Author:/', $out) === 0;
	}

	/**
	 * Retrieve a reviewer's attachment of the current submission.
	 *
	 * @param type $filename the name of the original filename
	 * @param type $output the name of the output file
	 */
	private function downloadReviewerAttachment($filename, $output) {
		$this->downloadSubmissionFile($filename, $output, $this->author);
	}

	/**
	 * Retrieve a file from the current submission.
	 *
	 * @param type $filename the original filename
	 * @param type $output the name of the output file
	 * @param type $username the user downloading the file
	 */
	private function downloadSubmissionFile($filename, $output, $username) {
		// find download link and extract url
		$fileXPath = '//a[contains(text(),\'' . $filename . '\')]';
		$this->waitForElementPresent($fileXPath);
		$downloadLinkHref = $this->getAttribute($fileXPath . '/@href');

		// use curl to actually get the file contents
		$curlCh = curl_init ();
		curl_setopt($curlCh, CURLOPT_POST, true);
		$cookies = tempnam (sys_get_temp_dir(), 'curlcookies');
		// log in
		$loginUrl = self::$baseUrl . '/index.php/publicknowledge/login/signIn';
		$loginParams = array(
			'username' => $username,
			'password' => $username . $username
		);
		curl_setopt($curlCh, CURLOPT_URL, $loginUrl);
		curl_setopt($curlCh, CURLOPT_POSTFIELDS, $loginParams);
		curl_setopt($curlCh, CURLOPT_COOKIEJAR, $cookies);
		$this->assertTrue(curl_exec($curlCh));

		$file = fopen($output, 'w');
		// download to local temporary file
		curl_setopt($curlCh, CURLOPT_POST, false);
		curl_setopt($curlCh, CURLOPT_URL, $downloadLinkHref);
		curl_setopt($curlCh, CURLOPT_HEADER, false);
		curl_setopt($curlCh, CURLOPT_FILE, $file);
		$this->assertTrue(curl_exec($curlCh));
		fclose($file);
		// clean up
		curl_close($curlCh);
		unlink($cookies);
	}
}
