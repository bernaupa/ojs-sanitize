{**
 * sanitizeSettingsForm.tpl
 *
 * Sanitize plugin management form.
 *}

<script type="text/javascript">
	$(function() {ldelim}
		// Attach the form handler.
		$('#usageStatsSettingsForm').pkpHandler('$.pkp.controllers.form.AjaxFormHandler');
	{rdelim});
</script>

<form class="pkp_form" id="usageStatsSettingsForm" method="post" action="{url router=$smarty.const.ROUTE_COMPONENT op="manage" category="generic" plugin=$pluginName verb="save"}">
	{csrf}

	{include file="controllers/notification/inPlaceNotification.tpl" notificationId="usageStatsSettingsFormNotification"}

	{fbvFormArea id="sanitizeCommands" title="plugins.generic.sanitize.settings.commands"}
		{fbvFormSection description="plugins.generic.sanitize.settings.commandsDescription"}
			{fbvElement type="text" id="commandPdf" value=$commandPdf label="plugins.generic.sanitize.settings.commands.pdf"}
		{/fbvFormSection}
	{/fbvFormArea}
	{fbvFormButtons id="sanitizeSettingsFormSubmit" submitText="common.save" hideCancel=true}
</form>

