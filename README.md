# Sanitize plugin for OJS


## About

This plugin pre-processes the [PDF](https://en.wikipedia.org/wiki/Portable_Document_Format) review files before they are shared to authors, removing any personal information in the document properties.

**Note**:
It only strips information from PDF files.


## System requirements

This plugin has been tested on [OJS](https://pkp.sfu.ca/ojs/) version 3.1.1-4.


## Installation

To install:
 - unpack the plugin archive to OJS plugins/generic directory;
 - enable the plugin by ticking the checkbox for 'Sanitize' in the set of generic plugins available ('Management' > 'Website Settings' > 'Plugins' > 'Generic Plugins').


## Details

Any PDF review file that an editor chooses to share with an author is anonymized.

It strips the metadata using QPDF, generating a new PDF file that is automatically uploaded as a new revision.

Only this latest revision (sanitized) is made viewable by the author.

Alternatively a reviewer file can be explictly anonymized with a 'Sanitize' action in the lists files, either when reading a review or when selecting the files to share with the author.

The commands applied to the files are customizable ('Settings' > 'Website' > 'Plugins' > 'Generic Plugins' > 'Sanitize' > 'Settings').
The default command for a PDF file requires [QPDF](http://qpdf.sourceforge.net/).
