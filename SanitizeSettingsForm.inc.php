<?php

/**
 * @file SanitizeSettingsForm.inc.php
 *
 * @class SanitizeSettingsForm
 * @ingroup plugins_generic_sanitize
 *
 * @brief Form to modify the sanitize plugin settings.
 */

//import('lib.pkp.classes.form.Form');

class SanitizeSettingsForm extends Form {

	/** @var $plugin Plugin */
	var $plugin;

	/**
	 * Constructor
	 * @param $plugin object
	 */
	function __construct($plugin) {
		$this->plugin = $plugin;

		parent::__construct($plugin->getTemplatePath() . '/sanitizeSettingsForm.tpl');
		$this->addCheck(new FormValidatorPost($this));
		$this->addCheck(new FormValidatorCSRF($this));
	}

	function initData() {
		$context = Request::getContext();

		$this->setData('commandPdf', $this->plugin->getSetting($context->getId(), 'commandPdf'));
	}

	function readInputData() {
		$this->readUserVars(array(
			'commandPdf',
		));
	}

	/**
	 * @see Form::fetch()
	 */
	function fetch($request) {
		$templateMgr = TemplateManager::getManager($request);
		$templateMgr->assign('pluginName', $this->plugin->getName());
		return parent::fetch($request);
	}

	function execute() {
		$context = Request::getContext();
		$this->plugin->updateSetting($context->getId(), 'commandPdf', $this->getData('commandPdf'));
	}
}
