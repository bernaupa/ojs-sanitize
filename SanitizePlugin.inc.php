<?php

/**
 * @file SanitizePlugin.inc.php
 *
 * @class SanitizePlugin
 * @ingroup plugins_generic_sanitize
 *
 * @brief Plugin for automatically sanitizing reviewer's attachments
 */

import('lib.pkp.classes.plugins.GenericPlugin');

class SanitizePlugin extends GenericPlugin {

	//
	// Implement methods from Plugin
	//
	/**
	 * @copydoc Plugin::register()
	 */
	function register($category, $path, $mainContextId = null) {
		if (parent::register($category, $path, $mainContextId)) {
			if ($this->getEnabled($mainContextId)) {
				// register handlers
				HookRegistry::register('LoadComponentHandler', array($this, 'overrideGridHandlers'));
			}
			return true;
		}
		return false;
	}

	/**
	 * @copydoc Plugin::getDescription()
	 */
	public function getDescription() {
		return __('plugins.generic.sanitize.description');
	}

	/**
	 * @copydoc Plugin::getDisplayName()
	 */
	public function getDisplayName() {
		return __('plugins.generic.sanitize.displayName');
	}

	/**
	 * @copydoc Plugin::getTemplatePath()
	 */
	function getTemplatePath($inCore = false) {
		return parent::getTemplatePath($inCore) . 'templates/';
	}

	/**
	 * @copydoc Plugin::getContextSpecificPluginSettingsFile()
	 */
	function getContextSpecificPluginSettingsFile() {
		return $this->getPluginPath() . '/settings.xml';
	}

	/**
	 * @see PKPPlugin::manage()
	 */
	function manage($args, $request) {
		$this->import('SanitizeSettingsForm');
		switch($request->getUserVar('verb')) {
			case 'settings':
				$settingsForm = new SanitizeSettingsForm($this);
				$settingsForm->initData();
				return new JSONMessage(true, $settingsForm->fetch($request));
			case 'save':
				$settingsForm = new SanitizeSettingsForm($this);
				$settingsForm->readInputData();
				if ($settingsForm->validate()) {
					$settingsForm->execute();
					$notificationManager = new NotificationManager();
					$notificationManager->createTrivialNotification(
						$request->getUser()->getId(),
						NOTIFICATION_TYPE_SUCCESS,
						array('contents' => __('plugins.generic.sanitize.settings.saved'))
					);
					return new JSONMessage(true);
				}
				return new JSONMessage(true, $settingsForm->fetch($request));
		}
		return parent::manage($args, $request);
	}

	//
	// Implement template methods from GenericPlugin.
	//
	/**
	 * @see Plugin::getActions()
	 */
	function getActions($request, $verb) {
		$router = $request->getRouter();
		import('lib.pkp.classes.linkAction.request.AjaxModal');
		return array_merge(
			$this->getEnabled()?array(
				new LinkAction(
					'settings',
					new AjaxModal(
						$router->url($request, null, null, 'manage', null, array('verb' => 'settings', 'plugin' => $this->getName(), 'category' => 'generic')),
						$this->getDisplayName()
					),
					__('manager.plugins.settings'),
					null
				),
			):array(),
			parent::getActions($request, $verb)
		);
	}


	/**
	 * Inject custom handlers.
	 * @param $hookName string LoadComponentHandler
	 * @param $args array [
	 *  @option $component string component name
	 *  @option $op string
	 * ]
	 * @return boolean
	 * @see PKPComponentRouter::getRpcServiceEndpoint()
	 */
	function overrideGridHandlers($hookName, $args) {
		$component =& $args[0];
		switch($component) {
			case 'modals.editorDecision.EditorDecisionHandler':
			case 'grid.files.attachment.EditorReviewAttachmentsGridHandler':
			case 'grid.files.attachment.EditorSelectableReviewAttachmentsGridHandler':
				$component = 'plugins.generic.sanitize.Sanitize' . array_pop(explode('.', $component));
				return true;

			case 'plugins.generic.sanitize.SanitizeHandler':
				return true;
		}
		return false;
	}

	/**
	 * Sanitize a submission file and save as new upload as new version
	 * 
	 * @param $submissionFile SubmissionFile the file to sanitize
	 */
	public static function sanitizePDF($submissionFile) {
		assert($submissionFile->getDocumentType() == DOCUMENT_TYPE_PDF);

		$submissionId = $submissionFile->getSubmissionId();
		$submissionDao = Application::getSubmissionDAO();
		$submission = $submissionDao->getById($submissionId);

		$pluginSettingsDao = DAORegistry::getDAO('PluginSettingsDAO'); /* @var $pluginSettingsDao PluginSettingsDAO */
		$cmd = $pluginSettingsDao->getSetting($submission->getContextId(), get_called_class(), 'commandPdf');
		if (empty($cmd)) {
			return;
		}

		$submissionFileDao = DAORegistry::getDAO('SubmissionFileDAO'); /* @var $submissionFileDao SubmissionFileDAO */

		$request = Application::getRequest();
		$user = $request->getUser();
		$userId = $user->getId();

		$fileId = $submissionFile->getFileId();

		import('lib.pkp.classes.file.TemporaryFileManager');
		$temporaryFileManager = new TemporaryFileManager();
		import('lib.pkp.classes.file.SubmissionFileManager');
		$submissionFileManager = new SubmissionFileManager($submission->getContextId(), $submissionId);

		// export as temporary file for processing
		$temporaryFile = $temporaryFileManager->submissionToTemporaryFile($submissionFile, $userId);

		// create copy of file
		$tmpfile = tempnam(sys_get_temp_dir(), 'sanitize');
		assert($tmpfile != FALSE);
		$ret = copy($temporaryFile->getFilePath(), $tmpfile);
		assert($ret);
		// sanitize the PDF
		$cmd = str_replace('{$in}',  escapeshellarg($tmpfile), $cmd);
		$cmd = str_replace('{$out}', escapeshellarg($temporaryFile->getFilePath()), $cmd);
		$output = $return_var = null;
		exec($cmd, $output, $return_var);
		if ($return_var != 0) {
			fatalError("Failed to convert PDF file \"". $submissionFile->getOriginalFileName() ."\" (command: \"". $cmd ."\").");
		}
		// clean up
		unlink($tmpfile);

		// import as revision
		$sanitizedFile = $submissionFileManager->copySubmissionFile($temporaryFile->getFilePath(), null, $userId, $fileId); /* @var $sanitizedFile SubmissionFile */
		// keep filename and display name from revision 1
		$sanitizedFile->setOriginalFileName($submissionFile->getOriginalFilename());
		$submissionLocale = $submissionFile->getSubmissionLocale();
		$sanitizedFile->setName($submissionFile->getName($submissionLocale), $submissionLocale);
		$submissionFileDao->updateObject($sanitizedFile);
		$temporaryFileManager->deleteFile($temporaryFile->getId(), $userId);

		// insert entry in the file log
		import('lib.pkp.classes.log.SubmissionFileLog');
		import('lib.pkp.classes.log.SubmissionFileEventLogEntry');
		SubmissionFileLog::logEvent(
				$request,
				$submissionFile,
				SUBMISSION_LOG_FILE_REVISION_UPLOAD,
				'plugins.generic.sanitize.submission.event.revisionUploaded',
				array(
						'fileId' => $fileId,
						'fileRevision' => $submissionFileDao->getLatestRevisionNumber($fileId),
						'originalFileName' => $submissionFile->getOriginalFileName(),
						'submissionId' => $submissionFile->getSubmissionId(),
						'username' => $user->getUsername()
				)
		);
	}
}
