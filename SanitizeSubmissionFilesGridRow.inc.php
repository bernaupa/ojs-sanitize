<?php

/**
 * @file SanitizeSubmissionFilesGridRow.inc.php
 *
 * @class SanitizeSubmissionFilesGridRow
 * @ingroup plugins_generic_sanitize
 *
 * @brief A grid row for submission file with sanitize action.
 *
 * @see lib/pkp/controllers/grid/files/SubmissionFilesGridRow.inc.php
 */

class SanitizeSubmissionFilesGridRow extends SubmissionFilesGridRow {
	//
	// Overridden template methods from GridRow
	//
	/**
	 * @copydoc PKPHandler::initialize()
	 */
	function initialize($request, $template = 'controllers/grid/gridRow.tpl') {
		parent::initialize($request, $template);

		$submissionFileData =& $this->getData();
		$submissionFile =& $submissionFileData['submissionFile']; /* @var $submissionFile SubmissionFile */
		import('plugins.generic.sanitize.SanitizeFileLinkAction');
		$this->addAction(new SanitizeFileLinkAction($request, $submissionFile, $this->getStageId()));
	}

	/**
	 * @copydoc GridRow::getActions()
	 */
	function getActions($position = GRID_ACTION_POSITION_DEFAULT) {
		$actions = parent::getActions($position);

		if ($position != GRID_ACTION_POSITION_DEFAULT) {
			// passthrough
			return $actions;
		}

		// reorderactions : sanitize before delete
		$sanitize = $actions['sanitizeFile'];
		unset($actions['sanitizeFile']);
		$position = array_search('deleteFile', array_keys($actions));
		return array_merge(
				array_slice($actions, 0, $position),
				array('sanitizeFile' => $sanitize),
				array_slice($actions, $position)
		);
	}
}
