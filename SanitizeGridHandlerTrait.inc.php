<?php

/**
 * @file SanitizeGridHandlerTrait.inc.php
 *
 * @class SanitizeGridHandlerTrait
 * @ingroup plugins_generic_sanitize
 *
 * @brief Extension for submission file grid supporting sanitization.
 *
 * @see lib/pkp/controllers/grid/files/SubmissionFilesGridHandler.inc.php
 * @see lib/pkp/classes/controllers/grid/GridHandler.inc.php
 */

import('plugins.generic.sanitize.SanitizeSubmissionFilesGridRow');

trait SanitizeGridHandlerTrait {
	/**
	 * @copydoc GridHandler::getRowInstance()
	 * @@return SanitizeSubmissionFilesGridRow
	 */
	protected function getRowInstance() {
		return new SanitizeSubmissionFilesGridRow($this->getCapabilities(), $this->getStageId());
	}
}
