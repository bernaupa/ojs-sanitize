<?php

/**
 * @file SanitizeEditorDecisionHandler.inc.php
 *
 * @class SanitizeEditorDecisionHandler
 * @ingroup plugins_generic_sanitize
 *
 * @brief Override of the regular handler for editors to make a decision
  */

import('controllers.modals.editorDecision.EditorDecisionHandler');

class SanitizeEditorDecisionHandler extends EditorDecisionHandler {
	/**
	 * @copydoc EditorDecisionHandler::_resolveEditorDecisionForm()
	 */
	protected function _resolveEditorDecisionForm($formName) {
		switch($formName) {
			case 'SanitizeSendReviewsForm':
				// redirect to custom form
				return 'plugins.generic.sanitize.SanitizeSendReviewForm';
			case 'SanitizePromoteForm':
				// redirect to custom form
				return 'plugins.generic.sanitize.SanitizePromoteForm';
			default:
				return parent::_resolveEditorDecisionForm($formName);
		}
	}

	/**
	 * @copydoc PKPEditorDecisionHandler::sendReviewsInReview()
	 */
	function sendReviewsInReview($args, $request) {
		// replace form with custom form
		return $this->_initiateEditorDecision($args, $request, 'SanitizeSendReviewsForm');
	}

	/**
	 * @copydoc PKPEditorDecisionHandler::saveSendReviewsInReview()
	 */
	function saveSendReviewsInReview($args, $request) {
		// replace form with custom form
		return $this->_saveEditorDecision($args, $request, 'SanitizeSendReviewsForm');
	}

	/**
	 * @copydoc PKPEditorDecisionHandler::promoteInReview()
	 */
	function promoteInReview($args, $request) {
		// replace with custom form
		return $this->_initiateEditorDecision($args, $request, 'SanitizePromoteForm');
	}

	/**
	 * @copydoc PKPEditorDecisionHandler::_saveEditorDecision()
	 */
	function _saveEditorDecision($args, $request, $formName, $redirectOp = null, $decision = null) {
		// hack to replace the form during savePromoteInReview()
		if ($formName == 'PromoteForm' && $redirectOp == WORKFLOW_STAGE_PATH_EDITING) {
			$formName = 'SanitizePromoteForm';
		}
		return parent::_saveEditorDecision($args, $request, $formName, $redirectOp, $decision);
	}

}
