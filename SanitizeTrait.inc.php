<?php

/**
 * @file SanitizeTrait.inc.php
 *
 * @class SanitizeTrait
 * @ingroup plugins_generic_sanitize
 *
 * @brief Trap in the editor decision process for sanitizing reviewer attachments
 *
 * @see lib/pkp/controllers/modals/editorDecision/form/EditorDecisionWithEmailForm.inc.php
 */

trait SanitizeTrait {
	/**
	 * @copydoc EditorDecisionWithEmailForm::_sendReviewMailToAuthor
	 */
	function _sendReviewMailToAuthor($submission, $emailKey, $request) {
		// pre-process the attachements before sending mail
		$selectedAttachments = $this->getData('selectedAttachments');
		if(is_array($selectedAttachments)) {
			$submissionFileDao = DAORegistry::getDAO('SubmissionFileDAO'); /* @var $submissionFileDao SubmissionFileDAO */

			foreach ($selectedAttachments as $fileId) {
				$submissionFile = $submissionFileDao->getLatestRevision($fileId);
				assert(is_a($submissionFile, 'SubmissionFile'));

				if ($submissionFile->getRevision() > 1) {
					// review file can usually not have revisions, so assume already sanitized
					continue;
				}

				if ($submissionFile->getDocumentType() != DOCUMENT_TYPE_PDF) {
					// can sanitize only PDFs
					continue;
				}

				SanitizePlugin::sanitizePDF($submissionFile);
			}
		}

		parent::_sendReviewMailToAuthor($submission, $emailKey, $request);
	}
}
