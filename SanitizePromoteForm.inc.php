<?php

/**
 * @file SanitizePromoteForm.inc.php
 *
 * @class SanitizePromoteForm
 * @ingroup plugins_generic_sanitize
 *
 * @brief Form for promoting a submission to editing.
 */

import('lib.pkp.controllers.modals.editorDecision.form.PromoteForm');
import('plugins.generic.sanitize.SanitizeTrait');

class SanitizePromoteForm extends PromoteForm {
	// insert sanitization
	use SanitizeTrait;
}
